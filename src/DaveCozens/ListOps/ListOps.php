<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DaveCozens\ListOps;

use pocketmine\plugin\PluginBase;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
//use pocketmine\event\TranslationContainer;
use pocketmine\Player;
use pocketmine\command\ConsoleCommandSender;
//use pocketmine\event\TranslationContainer;
use pocketmine\utils\TextFormat;


class ListOps extends PluginBase{
 
    public function onEnable(){
        $this->getLogger()->info("onEnable() ListOps  has been called!");
        
    }

    public function onDisable(){
        $this->getLogger()->info("onDisable() ListOps has been called!");
        
    }    

    public function onCommand(CommandSender $sender, Command $command, $label, array $args){
        if(strtolower($command->getName()) === "listops"){
            // Execute logic
            $this->getLogger()->info("ListOps has been called!");

                                        $result = "";
					$count = 0;
					foreach($sender->getServer()->getOnlinePlayers() as $player){
                                            $this->getLogger()->info($player->isOp());
                                            if($player->isOp()){
                                                $result .= $player->getName() . ", ";                                                
						++$count;
                                            }


					}
                                        if ($count==0){
                                            $sender->sendMessage(TextFormat::DARK_GREEN . "No Ops are currently online ");
                                        } else {
                                            $sender->sendMessage(TextFormat::DARK_GREEN . "Current Ops: " . TextFormat::WHITE . substr($result,-1));                                                                                        
                                        }
   					return true;
            
            
        }

        return false;
    }    
    
}

?>
